CompanyInfo
===========

公司登記查詢

資料來源為g0v開放資料
http://gcis.nat.g0v.tw

另外還有一個官方可以串接的Api
http://www.etax.nat.gov.tw/etwmain/front/ETW144GetPulicInfo?ban=<統編>
這個api只會回傳三個資訊：
負責人、公司名稱、公司登記位址
