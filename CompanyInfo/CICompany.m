//
//  CICompany.m
//  CompanyInfo
//
//  Created by HungChun on 2014/4/9.
//  Copyright (c) 2014年 HungChun. All rights reserved.
//

#import "CICompany.h"

@implementation CICompany

- (id)initWithJSON:(NSDictionary *)json {
    if (self = [super init]) {
        if ([json objectForKey:@"error"]) {
            _errorMsg = [json objectForKey:@"message"];
        } else {
            _errorMsg = nil;
            NSDictionary *jsonData = [json objectForKey:@"data"];
            if ([jsonData objectForKey:@"名稱"]) {
                _name = [jsonData objectForKey:@"名稱"];
                _businessScopeData = [NSArray array];
                _owner = @"N/A";
                _address = @"N/A";
                _totalCapital = @"N/A";
                _registrationAuthority = @"N/A";
                _date = @"N/A";
            } else {
                _name = [jsonData objectForKey:@"公司名稱"];
                _businessScopeData = [jsonData objectForKey:@"所營事業資料"];
                _owner = [jsonData objectForKey:@"代表人姓名"];
                _address = [jsonData objectForKey:@"公司所在地"];
                _totalCapital = [jsonData objectForKey:@"資本總額(元)"];
                _registrationAuthority = [jsonData objectForKey:@"登記機關"];
                NSString *month = [jsonData valueForKeyPath:@"核准設立日期.month"];
                NSString *day = [jsonData valueForKeyPath:@"核准設立日期.day"];
                NSString *year = [jsonData valueForKeyPath:@"核准設立日期.year"];
                _date = [NSString stringWithFormat:@"%@-%@-%@",year,month,day];
            }
        }
    }
    
    return self;
}

@end
