//
//  CIWebServices.m
//  CompanyInfo
//
//  Created by HungChun on 2014/4/9.
//  Copyright (c) 2014年 HungChun. All rights reserved.
//

#import "CIWebServices.h"
#import <RACEXTScope.h>

@interface CIWebServices ()

@property (nonatomic, copy) NSURLSession *client;

@end

@implementation CIWebServices

+ (CIWebServices *)sharedServices {
    static CIWebServices *service = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[CIWebServices alloc] init];
        service.client = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    });
    return service;
}

- (RACSignal *)fetchJSONFromURL:(NSURL *)url {
    @weakify(self);
    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        NSURLSessionDataTask *task = [self.client dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error) {
                NSError *jsonError = nil;
                id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
                if (! jsonError) {
                    [subscriber sendNext:json];
                } else {
                    [subscriber sendError:jsonError];
                }
            } else {
                [subscriber sendError:error];
            }
            [subscriber sendCompleted];
        }];
        [task resume];
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }];
    return signal;
}

- (RACSignal *)fetchCompanyInformationWithNumber:(NSString *)number {
    NSString *relatedURLString = [NSString stringWithFormat:@"http://gcis.nat.g0v.tw/api/show/%@",number];
    NSURL *url = [NSURL URLWithString:relatedURLString];
    return [[self fetchJSONFromURL:url] map:^id(id result) {
        return [[CICompany alloc] initWithJSON:result];
    }];
}

@end
