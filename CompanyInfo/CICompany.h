//
//  CICompany.h
//  CompanyInfo
//
//  Created by HungChun on 2014/4/9.
//  Copyright (c) 2014年 HungChun. All rights reserved.
//


@interface CICompany : NSObject

//公司名稱
@property (nonatomic, strong) NSString *name;

//所營事業資料
@property (nonatomic, strong) NSArray *businessScopeData;

//負責人
@property (nonatomic, strong) NSString *owner;

//登記地點
@property (nonatomic, strong) NSString *address;

//資本額
@property (nonatomic, strong) NSString *totalCapital;

//登記機關
@property (nonatomic, strong) NSString *registrationAuthority;

//設立日期
@property (nonatomic, strong) NSString *date;

//統編
@property (nonatomic) NSInteger number;

@property (nonatomic, strong) NSString *errorMsg;

- (id)initWithJSON:(NSDictionary *)json;

@end
