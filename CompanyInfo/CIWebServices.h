//
//  CIWebServices.h
//  CompanyInfo
//
//  Created by HungChun on 2014/4/9.
//  Copyright (c) 2014年 HungChun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>
#import "CICompany.h"

@interface CIWebServices : NSObject <NSURLSessionDataDelegate>

+ (CIWebServices *)sharedServices;

- (RACSignal *)fetchCompanyInformationWithNumber:(NSString *)number;

@end
