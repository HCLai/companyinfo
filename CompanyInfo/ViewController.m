//
//  ViewController.m
//  CompanyInfo
//
//  Created by HungChun on 2014/4/9.
//  Copyright (c) 2014年 HungChun. All rights reserved.
//

#import "ViewController.h"
#import "CIWebServices.h"
#import <RACEXTScope.h>
#import <TSMessage.h>
#import <SVProgressHUD.h>
@interface ViewController ()
{
    NSDictionary *cellAttributes_;
}
@property (nonatomic, strong)   CICompany *companyInfo;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableParagraphStyle* para = [NSMutableParagraphStyle new];
    para.lineBreakMode = NSLineBreakByWordWrapping;
    cellAttributes_ = [NSDictionary dictionary];
    cellAttributes_ = @{NSFontAttributeName : [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:16.0], NSParagraphStyleAttributeName : para, NSForegroundColorAttributeName : [UIColor darkGrayColor]};
    
    RACSignal *numberSignal = [[RACSignal combineLatest:@[self.textField.rac_textSignal]] reduceEach:^id (NSString *number){
        return @(number.length == 8);
    }];
    
    RAC(self,searchButton.enabled) = numberSignal;
    
    @weakify(self);
    [[self.searchButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        [self.textField resignFirstResponder];
        [SVProgressHUD show];
        [[[[CIWebServices sharedServices] fetchCompanyInformationWithNumber:_textField.text] deliverOn:RACScheduler.mainThreadScheduler] subscribeNext:^(CICompany *company) {
            [SVProgressHUD dismiss];
            self.companyInfo = company;
            if (company.errorMsg) {
                [TSMessage showNotificationInViewController:self title:@"社團和財團法人暫無資料" subtitle:nil type:TSMessageNotificationTypeWarning];
            } else {
                [self.tableView reloadData];
            }
        } error:^(NSError *error) {
            [SVProgressHUD dismiss];
            [TSMessage showNotificationInViewController:self title:@"伺服器掛了..." subtitle:nil type:TSMessageNotificationTypeError];
        }];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!_companyInfo) {
        return 0;
    } else {
        if (_companyInfo.errorMsg)
            return 0;
        else
            return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 7;
    } else if(section == 1) {
        return self.companyInfo.businessScopeData.count;
    } return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor blackColor];
    label.alpha = 0.5;
    if (section == 0) {
        label.text = @"基本資料";
    } else if(section == 1) {
        label.text = @"所營事業資料";
    }
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:@"公司名稱" attributes:cellAttributes_];
            cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:_companyInfo.name attributes:cellAttributes_];
        } else if (indexPath.row == 1) {
            cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:@"負責人" attributes:cellAttributes_];
            cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:_companyInfo.owner attributes:cellAttributes_];
        } else if (indexPath.row == 2) {
            cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:@"統一編號" attributes:cellAttributes_];
            cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:_textField.text attributes:cellAttributes_];
        } else if (indexPath.row == 3) {
            cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:@"住址" attributes:cellAttributes_];
            cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:_companyInfo.address attributes:cellAttributes_];
        } else if (indexPath.row == 4) {
            cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:@"登記機關" attributes:cellAttributes_];
            cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:_companyInfo.registrationAuthority attributes:cellAttributes_];
        } else if (indexPath.row == 5) {
            cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:@"設立日期" attributes:cellAttributes_];
            cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:_companyInfo.date attributes:cellAttributes_];
        } else if (indexPath.row == 6) {
            cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:@"資本總額" attributes:cellAttributes_];
            cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:_companyInfo.totalCapital attributes:cellAttributes_];
        }
    } else {
        NSArray *value = [_companyInfo.businessScopeData objectAtIndex:indexPath.row];
        cell.textLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:[value objectAtIndex:0] attributes:cellAttributes_];
        cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:[value objectAtIndex:1] attributes:cellAttributes_];
    }
    return cell;
}


@end
