//
//  AppDelegate.h
//  CompanyInfo
//
//  Created by HungChun on 2014/4/9.
//  Copyright (c) 2014年 HungChun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
